import setuptools

with open("README.md", "r") as fh:
    long_description = fh.read()

setuptools.setup(
    name="templatelib",
    version="0.1",
    author="Andrei Papkou",
    author_email="aapsonn@gmail.com",
    description="A tool for designing recombination oligos to make a mutant library",
    long_description=long_description,
    long_description_content_type="text/markdown",
    url="https://gitlab.com/apson/templatelib",
    # entry_points = {
    #    "console_scripts": ['get_oligos_for_editing = templatelib.get_oligos_for_editing:main']
    #   },
    include_package_data=True,
    package_dir={"templatelib": "templatelib"},
    package_data={"templatelib": ["data/codon_usage_by_codon.pkl"]},
    packages=["templatelib", "templatelib.helpers"],
    data_files=[
        (
            "templatelib",
            [
                "templatelib/data/codon_dist_long.csv",
                "templatelib/data/codon_dist_long.pkl",
                "templatelib/data/codon_usage.csv",
                "templatelib/data/codon_usage_extra_data.csv",
                "templatelib/data/codon_usage_by_aa.pkl",
            ],
        ),
    ],
    classifiers=[
        "Programming Language :: Python :: 3",
        "License :: GNU General Public License v3.0.",
        "Operating System :: OS Independent",
    ],
    python_requires=">=3.6",
)
