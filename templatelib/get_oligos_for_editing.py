#!/usr/bin/env python3

"""
Design recombination oligos for gene-editing

Parameters
-----------
To see arguments run get_oligos_for_editing.py --help

Return
------------
A list of candidate oligos in fasta format ranked according to dG,length,GC  
"""

## generic libraries
import sys
import argparse
import multiprocessing as mp

## bio libraries
from Bio.SeqRecord import SeqRecord
from Bio import SeqIO



## custom functions

from templatelib.helpers.mutations import mutate_seq_from_primer, make_point_mutation
from templatelib.helpers.mutations import mutation_string_to_dic
from templatelib.helpers.primers_create_analyze import (
    create_primer_variants,
    analyze_primer, 
)
from templatelib.helpers.primers_create_analyze import (
    rank_primers,
    analysed_list_primers_to_seq,
)

############
## parse arguments
############

parser = argparse.ArgumentParser(
    "Make primers with mismatch mutations PAM for genetic editing"
)

parser.add_argument(
    "--fasta-gRNA",
    metavar="FILE",
    help="path to fasta file with one or more gRNAs (including PAM! at 3",
)

parser.add_argument(
    "--fasta-gene", metavar="FILE", help="path to gene sequence in fasta format"
)

parser.add_argument(
    "--gRNA", metavar="STR", help="a name of gRNA as in the fasta file, e.g. folA_gR38f"
)

parser.add_argument(
    "--modified-pam",
    metavar="STR",
    help="a three-letter sequence of destroyed PAM sequence. If PAM is not be modified, provide the unmodified sequence",
)

parser.add_argument(
    "--dna-mutation",
    metavar="STR",
    help="a DNA mutation in a colloquial format, eg. T83G",
)

parser.add_argument(
    "--min-max-size",
    metavar="MIN,MAX",
    help="size of oligos given as min and max value separated by comma, eg. 45,46 ",
)

parser.add_argument(
    "--flexibility",
    metavar="INT",
    help="a maximum number of nucleotide postions from oligo's center the mutation can be located",
)


args = parser.parse_args()


# out_handle=sys.stdout
# fasta_gRNA="/media/DATA/ngs/EC/ec04.dhfr_crispr/out_data/crispr_search1/folAallgRNAs.fa"
# fasta_gene="/media/DATA/ngs/EC/common/genes/folA.fasta"
# # gRNA="folA_gR75r"
# gRNA="folA_gR92r"
# modified_pam="AAG"
# dna_mutation="T83G"
# min_max_size_str='45,55'
# flexibility=5


###############
## functions
##############


def parse_gRNA(gRNA, fasta):
    """
    Read gRNAs from a fasta file

    Parameters
    --------------
    gRNA: str
        name of gRNA as in fasta file
    fasta: str
        path to a file with gRNA(s)
    
    Return
    --------------
    SeqRecord object with gRNA sequence
    """
    #IUPAC.IUPACAmbiguousDNA()
    for seq_rec in SeqIO.parse(fasta, "fasta"):
        if gRNA == seq_rec.name:
            return seq_rec


#################
## modify PAM and mutate gene accordingly
#################


def modify_pam(gRNA_seq, modified_pam):

    """
    Change gRNA sequence to modify PAM

    
    Note:
    --------------
    PAM (protospacer adjucent motif) is not part of gRNA, but here it is attached for convenience 

    Parameters
    --------------
    gRNA_seq: SeqRecord
        an object with gRNA sequence and PAM
    modified_pam: str
        a three nucleotide sequence of modified PAM
    
    Return
    --------------
    SeqRecord object of gRNA sequence with modified PAM
    """

    if gRNA_seq[-2:].seq != "GG":
        exit("No valid PAM found at 3'")
    else:
        gRNA_pam_modified = gRNA_seq.seq.tomutable()
        gRNA_pam_modified[-3:] = modified_pam
        return SeqRecord(gRNA_pam_modified.toseq(), id="gRNA_destroyed_pam")


##############
## rank and return primers
###############


def main(argv):

    # parse arguments

    fasta_gRNA = args.fasta_gRNA
    fasta_gene = args.fasta_gene
    gRNA = args.gRNA
    modified_pam = args.modified_pam
    dna_mutation = args.dna_mutation
    min_max_size_str = args.min_max_size
    min_max_size = [int(a) for a in min_max_size_str.rsplit(sep=",")]
    flexibility = int(args.flexibility)

    ##################
    # read and mutate gene
    ##################

    # read input sequences

    gRNA_seq = parse_gRNA(gRNA, fasta_gRNA)
    # IUPAC.IUPACAmbiguousDNA()
    gene = SeqIO.read(fasta_gene, "fasta")

    # modify PAM and cahnge gene accordingly

    gRNA_seq_modified_pam = modify_pam(gRNA_seq, modified_pam)
    gene_destroyed_pam = mutate_seq_from_primer(
        gene, gRNA_seq_modified_pam, suffix="destroyed PAM"
    )

    # introduce point mutation in gene sequence

    gene_destroyed_pam_mutated = make_point_mutation(gene_destroyed_pam, dna_mutation)
    mutation_position = mutation_string_to_dic(dna_mutation)["position"]

    # make sure PAM-modification does not cause amino acid substitution, and that DNA mutation does

    if str(gene_destroyed_pam.translate()) != str(gene.translate()):
        print("Warning PAM modification result into AA substitution")

    if str(gene_destroyed_pam_mutated.translate()) == str(gene.translate()):
        print("Warning PAM modification result into AA substitution")

    ##################
    # design oligos
    ##################

    # generate a list of all possible oligos based on mix and man length

    list_of_primers = create_primer_variants(
        gene_destroyed_pam_mutated,
        mutation_position=mutation_position,
        min_max_size=min_max_size,
        mutation_position_flexibility=flexibility,
    )

    # analyze primers in parallel (GC, Tm, deltaG. length)

    pool = mp.Pool(mp.cpu_count())
    list_of_primers_analysed = pool.map(
        analyze_primer, [aprimer for aprimer in list_of_primers]
    )
    pool.close()

    # rank primers based on analysis results

    list_of_primers_analysed_ranked = rank_primers(list_of_primers_analysed)

    # return primers
    SeqIO.write(
        analysed_list_primers_to_seq(list_of_primers_analysed_ranked),
        sys.stdout,
        "fasta",
    )

    pass


if __name__ == "__main__":
    main(args)
