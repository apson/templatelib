"""
This script was only ran once to save E. coli codon usage as dictionary (in a pickle).
I kept this file in case another organism or other data source will be used 
"""

import pkg_resources
import csv
import pickle

# get path to the data distributed with the package
filepath = pkg_resources.resource_filename('templatelib', 'data/codon_usage.csv')

# parse csv into dict
with open(filepath, mode='r') as infile:
    reader = csv.reader(infile)
    mydict = {rows[0]:float(rows[1]) for rows in reader if rows[0] != 'Codon'}
    
# >> mydict
# {'Codon': 'Usage', 'AAA': '0.739633210382827', 'AAC': '0.509770663963244', 'AAG': '0.260366789617173', 'AAT': '0.490229336036756', 'ACA': '0.167400576596501', 'ACC': '0.397629438015876', 'ACG': '0.248158643023577', 'ACT': '0.186811342364046', 'AGA': '0.0652497469786271', 'AGC': '0.244754720971423', 'AGG': '0.037739874183882', 'AGT': '0.159041442330569', 'ATA': '0.1131255320305', 'ATC': '0.39300527049789', 'ATG': '1', 'ATT': '0.49386919747161', 'CAA': '0.338869672833434', 'CAC': '0.426204084695524', 'CAG': '0.661130327166566', 'CAT': '0.573795915304476', 'CCA': '0.203203938550366', 'CCC': '0.12672698216311', 'CCG': '0.493891122796165', 'CCT': '0.17617795649036', 'CGA': '0.0686828997241571', 'CGC': '0.358258419162152', 'CGG': '0.106923855450378', 'CGT': '0.363145204500804', 'CTA': '0.0412781243888914', 'CTC': '0.100046343795959', 'CTG': '0.474715842881138', 'CTT': '0.116392577491917', 'GAA': '0.676314622432323', 'GAC': '0.369846532130529', 'GAG': '0.323685377567677', 'GAT': '0.630153467869471', 'GCA': '0.228715105218917', 'GCC': '0.261671410243176', 'GCG': '0.325339546681217', 'GCT': '0.18427393785669', 'GGA': '0.129464900238475', 'GGC': '0.369766844872038', 'GGG': '0.153565409556124', 'GGT': '0.347202845333363', 'GTA': '0.165246116209857', 'GTC': '0.204274895990776', 'GTG': '0.347729716253486', 'GTT': '0.282749271545881', 'TAA': '0.614639037433155', 'TAC': '0.411087981725739', 'TAG': '0.0826370320855615', 'TAT': '0.588912018274261', 'TCA': '0.144140532062071', 'TCC': '0.147290783641603', 'TCG': '0.137227602372161', 'TCT': '0.167544918622172', 'TGA': '0.302723930481283', 'TGC': '0.536901708367614', 'TGG': '1', 'TGT': '0.463098291632386', 'TTA': '0.140322584965858', 'TTC': '0.420508124119082', 'TTG': '0.127244526476237', 'TTT': '0.579491875880918'}
# >>> 

# remove header record



def save_object(obj, filename):
    """
    Save object as a picklefile

    Parameters
    --------------
    obj: object
    filename: str
        output file path
        
    """
    with open(filename, 'wb') as output:  # Overwrites any existing file.
        pickle.dump(obj, output, pickle.HIGHEST_PROTOCOL)

# save dictionary as pickle 
save_object(mydict, 'out_data/codon_usage_by_codon.pkl')


filepath2 = pkg_resources.resource_filename('templatelib', 'data/codon_usage_extra_data.csv')

# get unique AA
aa=[]
with open(filepath2, mode='r') as infile:
    for aline in csv.reader(infile):
        if aline[1]!='AA':
            aa.append(aline[1])


# get dictionary 
res={}
for amino_acid in set(aa):
    with open(filepath2, mode='r') as infile:
        reader = csv.reader(infile)
        mydict = {rows[0]:float(rows[5]) for rows in reader if rows[1]==amino_acid}
        res.update( {amino_acid : mydict} )

# res['R']

save_object(res, 'out_data/codon_usage_by_aa.pkl')
