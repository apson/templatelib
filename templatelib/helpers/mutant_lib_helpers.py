"""
Functions to create sets of mutants
"""

import pkg_resources
import pickle

# get path to the data distributed with the package
codon_dist_path = pkg_resources.resource_filename(
    "templatelib", "data/codon_dist_long.pkl"
)
codon_usage_by_codon_path = pkg_resources.resource_filename(
    "templatelib", "data/codon_usage_by_codon.pkl"
)
codon_usage_by_aa_path = pkg_resources.resource_filename(
    "templatelib", "data/codon_usage_by_aa.pkl"
)


def load_object(filename):
    """
    Load object a pickle file

    Parameters
    --------------
    filepath: str
       file path to a pickle file
        
    Return
    ______________
    obj: an object from pickel file
    
    """
    with open(filename, "rb") as myfile:
        return pickle.load(myfile)


def get_codon_dist_data(filepath=codon_dist_path):
    """
    Load codon distance data from a pickle file

    Parameters
    --------------
    filepath: str
        a file path to codon distance pickle file
    
    Return
    ______________
    dic: dictionary object from pickel file
        
    """
    obj = load_object(filepath)
    return obj


def get_dist(codon1, codon2, codon_dist_data=get_codon_dist_data()):
    """
    Show distance (number of substitutions) between 2 codon

    Parameters
    --------------
    codon1: str
        a DNA codon in a three letter format, ie AAA
    codon2: str
        another DNA codon, for example AAT 
    codon_dist_data: dic
        a dictionary oject containing record for each a pair of codons as a key and distance as a value, eg {'AAA_AAT':1}
        
    Return
    ______________
    int: a distance between codons ranging from 0 to 3 (as a minimum numbrer of mutation step, analogous to Hamming distance)
        
    """
    key = codon1 + "_" + codon2
    dist = codon_dist_data[key]
    return dist


# get_dist("AAA","TTT")


def get_codon_usage_data(filepath=codon_usage_by_codon_path):
    """
    Load codon usage data from a pickle file

    Parameters
    --------------
    filepath: str
        a file path to codon usage pickle file
    
    Return
    ______________
    dic: dictionary object from pickle file
        
    """
    obj = load_object(filepath)
    return obj


def get_codon_usage_data_aa(filepath=codon_usage_by_aa_path):
    """
    Load codon usage data from a pickle file

    Parameters
    --------------
    filepath: str
        a file path to codon usage pickle file
    
    Return
    ______________
    dic: dictionary object from pickle file
        
    """
    obj = load_object(filepath)
    return obj


def get_usage_one_codon(codon):
    """
    Get codon usage data for a single codon

    Parameters
    --------------
    codon: str
        DNA codon, eg 'ATG'
    
    Return
    ______________
    float: codon usage as a fracion
        
    """
    res = get_codon_usage_data(filepath=codon_usage_by_codon_path)[codon]
    return res


def get_usage_aa(aa, sort=True):
    """
    Get codon usage data for an amino acid
    
    Parameters
    --------------
    aa: str
        an amino acid using a single letter notation, eg W for tryptophane
    sorted: logic
        sort codons by their usage (from high to low). Default: True
    
    Return
    ______________
    lst of dic: list of all codons for amino acid along with usage data
        
    """
    res = get_codon_usage_data(filepath=codon_usage_by_aa_path)[aa]
    if sort:
        res_sorted = [
            {k: v}
            for k, v in sorted(res.items(), key=lambda item: item[1], reverse=True)
        ]
        return res_sorted
    else:
        return res


# get_usage_one_codon("AAA")
# get_usage_aa("A")

def mutate_codon_n_steps(codon,n_steps,include_stop=False):
    """
    Show all codons n steps away
    
    Parameters
    --------------
    codon: str
        a DNA codon , eg AGT
    n_steps: int
        number of mutational steps (distance)
    sorted: logic
        include stop codons. Default: False
    
    Return
    ______________
    lst: list of codons n steps away from the input codon
    
    """
    # first subset by distance
    subset_dist = { k:v for k,v in get_codon_dist_data().items() if n_steps == v }
    # and now subset by first codon 
    subset_codon=[ k for k,v in subset_dist.items() if str(codon + '_') in k ]
    if include_stop:
        # and rsplit it to return second codons
        res = [ s.rsplit('_')[1] for s in subset_codon ]
    else:
        # get combinations with stop codons to avoid
        stop_codons_to_remove = [codon + '_' + stop_codon for stop_codon in ['TAA','TGA','TAG'] ]
        # and rsplit it to return second codons
        res = [ s.rsplit('_')[1] for s in subset_codon if not s in stop_codons_to_remove ]
    return(res)



# len(mutate_codon_n_steps('AAA',2))
# len(mutate_codon_n_steps('AAA',2,include_stop=True))

