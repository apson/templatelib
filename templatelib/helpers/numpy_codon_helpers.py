"""
Functions to use numpy for manipulation of arrays representing mutant libraries

"""

import numpy as numpy
from templatelib.helpers.numpy_wrappers import *
from templatelib.helpers.mutant_lib_helpers import *

from Bio.Seq import Seq
from Bio.SeqRecord import SeqRecord



def seq_to_codon_numpy(seq_rec):
    """
    Convert sequence to numpy array using 3-letter codon as an element

    Parameters
    --------------
    seq_rec: SeqRecord
        a sequence to be converted

    Return
    --------------
    numpy array
    """
    return str_to_codon_numpy(seq_rec.seq)



def str_to_codon_numpy(seq_str):
    """
    Convert sequence string into numpy array using 3-letter codon as an element

    Parameters
    --------------
    seq_str: str
        a sequence given as a string object 

    Return
    --------------
    numpy array
    """
    #convert string itno 3 letter chunks
    sequence_chunks = [seq_str[i:i + 3] for i in range(0, len(seq_str), 3)]
    np_array = numpy.array(sequence_chunks, numpy.unicode_, order="F")
    # print("Array shape %i" % np_array.shape)
    return np_array


#seq='AAATAA'

# str_to_codon_numpy(seq)
# seq_to_codon_numpy(SeqRecord(seq))

def create_codon_variants(seq_rec,list_codons,codon_position):
    """
    Create multipe variants for coding sequence 

    Parameters
    --------------
    seq_rec: SeqRec
        a sequence record object
    list_codons: lst
        list of codons to create variants
    codon_position: int
        0-based index of the codon to be modified

    Return
    --------------
    2D numpy array 
    """
    # convert seq into 1D array
    array_1D = seq_to_codon_numpy(seq_rec)
    # create 2D array with multiple copies
    times=len(list_codons)
    array_2D = numpy.reshape(numpy.tile(array_1D,times),(times,array_1D.shape[0]))
    # modify each sequence
    for row in range(0,times,1):
        array_2D[row,codon_position] = list_codons[row]
    return array_2D


# a=create_codon_variants(seq_rec=SeqRecord(seq='AAAATA'),list_codons=['CGA','AAA','TCT'],codon_position=1)

# array_to_seq(a)


def create_gene_variants_nstep(seq_rec,aa_position,nsteps,outfmt='array'):
    """
    Create all possible codon variants for a given position in a gene

    Parameters
    --------------
    seq_rec: SeqRec
        a sequence record object
    aa_position: int
        1-based index of amino acid position to be affected
    nsteps: int
        number of mutational steps to diversify a codon
    outfmt: str ('array','SeqRecord')
        Specify format, 2D array or sequence record
          
    Return
    --------------
    SeqRecord
    """
    # get the codon to modify
    codon_position = aa_position -1
    codon_to_change = str(seq_to_codon_numpy(seq_rec) [codon_position])
    # get all alternative codons excluding stop codons
    new_codons = mutate_codon_n_steps(codon = codon_to_change,n_steps = nsteps, include_stop=False)
    
    array_2D = create_codon_variants(seq_rec = seq_rec,list_codons = new_codons, codon_position = codon_position)
    if outfmt =='array':
        return array_2D
    elif outfmt =='SeqRecord':
        return array_to_seq(array_2D, gapped=True)
    else:
        raise ValueError('check output format')



# create_gene_variants_nstep(seq_rec=SeqRecord(seq='AAAATA'),aa_position=1,nsteps=1)

# create_gene_variants_nstep(seq_rec=SeqRecord(seq='AAAATA'),aa_position=1,nsteps=1,outfmt='SeqRecord')

# create_gene_variants_nstep(seq_rec=SeqRecord(seq='AAAATA'),aa_position=1,nsteps=1,outfmt='SeqRecords')
