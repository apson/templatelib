"""
Functions to convert sequence data to numpy arrays and make use of numpy functionality

"""

import numpy as numpy

from Bio.Seq import Seq
from Bio.SeqRecord import SeqRecord



def seq_to_numpy(seq_rec):
    """
    Convert sequence to numpy array

    Parameters
    --------------
    seq_rec: SeqRecord
        a sequence to be converted

    Return
    --------------
    numpy array
    """
    np_array = numpy.array(seq_rec.seq, numpy.unicode_, order="F")
    # print("Array shape %i" % np_array.shape)
    return np_array


def str_to_numpy(seq_str):
    """
    Convert sequence string to numpy array

    Parameters
    --------------
    seq_str: str
        a sequence given as a string object 

    Return
    --------------
    numpy array
    """
    np_array = numpy.array(list(seq_str), numpy.unicode_, order="F")
    # print("Array shape %i" % np_array.shape)
    return np_array


def alignment_to_numpy(alignment):
    """
    Convert sequence alignment to numpy array

    Parameters
    --------------
    alignment: Alignment object
        a sequence alignment object with two of more sequneces to be converted

    Return
    --------------
    numpy array
    """
    np_array = numpy.array([list(rec) for rec in alignment], numpy.unicode_, order="F")
    # print("Array shape %i by %i" % np_array.shape)
    return np_array


# alignment_to_numpy(aa)


def ungap_alignment_array(array, seq_choose=1):
    """
    Remove gaps ('-') from one sequence in alignment array. It is useful to "zoom in" when an alignment contains a long and a short sequecne. 

    Parameters
    --------------
    alignment: numpy array
        an array of alignment with two of more sequneces
    seq_choose: 
        0 based index of sequnece to ungap.  

    Return
    --------------
    numpy array showing gaps removed for one sequence and the corresponding nucleotides for the other sequecnes
    """
    new_array = array[:, array[seq_index,] != "-"]
    return new_array


def get_diff_columns(array):
    """
    Show columns which are different between two sequences in the alignment.
    
    Note
    --------------
    This function ignores gaps!

    Parameters
    --------------
    array: numpy array
        an array of alignment with two sequneces

    Return
    --------------
    lst: list of columns which are different between the two sequences
    """
    result_list = []
    for col in range(array.shape[1]):
        if (
            (array[0, col] != array[1, col])
            & (array[1, col] != "-")
            & (array[0, col] != "-")
        ):
            result_list.append(col)
    return result_list


# get_diff_columns(alignment_to_numpy(ab))


def one_dimension_array_to_string(array, gapped=True):
    """
    Convert 1D array (i.e. with one sequnce) into sequence string
    
    Parameters
    --------------
    array: numpy array
        an array with one sequnece
    gapped: logic
        keep gaps ('-'). Default=True

    Return
    --------------
    str: a sequence string 
    """
    if array.ndim > 1:
        sys.exit("Function require a single dimention array")
    elif gapped:
        res = array.tostring().decode("utf-32")
    else:
        res = array[array != "-"].tostring().decode("utf-32")
    return res


# one_dimension_array_to_string(alignment_to_numpy(aa)[0],gapped=False)
# one_dimension_array_to_string(alignment_to_numpy(aa)[1],gapped=True)


def array_to_str(array, gapped=True):
    """
    Convert array into sequence(s) string
    
    Parameters
    --------------
    array: numpy array
        an array with one or more sequneces
    gapped: logic
        keep gaps ('-'). Default=True

    Return
    --------------
    str: if there is one sequence, returns one sequence string
    lst: if there are more than once sequecne, return a list of sequence strings 
    """
    res = []
    if array.ndim > 1:
        for seq in range(array.shape[0]):
            res.append(one_dimension_array_to_string(array[seq,], gapped=gapped))
        return res
    elif array.ndim == 1:
        res.append(one_dimension_array_to_string(array, gapped=gapped))
        return res[0]


# array_to_str(alignment_to_numpy(aa),gapped=False)
# array_to_str(alignment_to_numpy(aa)[1],gapped=True)


def array_to_seq(array, gapped=True):
    """
    Convert array into SeqRecord(s) object
    
    Parameters
    --------------
    array: numpy array
        an array with one or more sequneces
    gapped: logic
        keep gaps ('-'). Default=True

    Return
    --------------
    SeqRecord: if there is one sequence, returns one SeqRecord
    lst of SeqRecords: if there are more than once sequecne, return a list of SeqRecords
    """
    #IUPAC.IUPACAmbiguousDNA()
    res = []
    if array.ndim == 1:
        aseq_rec = SeqRecord(
            Seq(array_to_str(array, gapped=gapped)), id=""
        )
        res.append(aseq_rec)
        return res[0]
    elif array.ndim > 1:
        for aseq in array_to_str(array, gapped=gapped):
            aseq_rec = SeqRecord(Seq(aseq), id="")
            res.append(aseq_rec)
        return res


def ungap_seq(sec_rec):
    """
    Ungap SeqRecord
    
    Parameters
    --------------
    sec_rec: SeqRecord
        a sequecne to be ungapped

    Return
    --------------
    SeqRecord: return a SeqRecord without gaps
    """
    array = seq_to_numpy(sec_rec)
    res = array_to_seq(array, gapped=False)
    return res


