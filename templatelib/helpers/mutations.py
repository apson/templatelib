"""
Functions to deal with mutations

"""

import re
import sys

from Bio.Seq import Seq
from Bio.SeqRecord import SeqRecord
from Bio.Data import CodonTable

from templatelib.helpers.mafta_wrappers import get_alignment_mafta
from templatelib.helpers.numpy_wrappers import *


def mutate_seq_from_primer(initial_seq, primer_seq, suffix="mutated"):
    """
    Mutate sequence by using a homologous temple containing mutation

    Parameters
    --------------
    initial_seq: SeqRecord
        a sequence to be modified
    primer_seq: SeqRecord
        a homologous sequence containing a mutation to introduce into initial sequence
    suffix: str
        suffix to add to a name of resulting sequence

    Return
    --------------
    SeqRecord: object with modified sequence
    """

    # align primer to seq
    alignment = get_alignment_mafta(initial_seq, primer_seq)

    # convert to array
    array = alignment_to_numpy(alignment)

    # modify initial sequence
    for col in range(array.shape[1]):
        if (
            (array[0, col] != array[1, col])
            & (array[1, col] != "-")
            & (array[0, col] != "-")
        ):
            array[0, col] = array[1, col]

    # get modified system and rename using suffix
    myseq = array_to_seq(array[0], gapped=True)
    myseq.name = initial_seq.name + "_" + suffix

    return myseq


def mutation_string_to_dic(string):
    """
    Convert a string encoding DNA or AA mutation into dictionary

    Parameters
    --------------
    string: str
        a DNA or protein mutation encoded using colloquial notation e.g. A83C or L22R

    Return
    --------------
    dct: A dictionary object encoding a mutation
    """
    re_from = re.compile("^[a-zA-Z]")
    re_to = re.compile("[a-zA-Z]$")
    re_position = re.compile("[0-9]+")
    from_ = re_from.findall(string)[0]
    to = re_to.findall(string)[0]
    position = int(re_position.findall(string)[0]) - 1
    return {"from": from_, "to": to, "position": position}


def make_point_mutation(initial_seq, mutation_string):
    """
    Mutate sequence using a string describing mutation

    Parameters
    --------------
    initial_seq: SeqRecord
        a sequence to be modified
    mutation_string: str
        a DNA or protein mutation encoded using colloquial notation e.g. A83C or L22R

    Return
    --------------
    SeqRecord: object with mutated sequence
    """
    # convert string to dictionary
    mut_dic = mutation_string_to_dic(mutation_string)
    # convert sequence to array and introduce mutation
    initial_array = seq_to_numpy(initial_seq)
    if initial_array[mut_dic["position"]].casefold() != mut_dic["from"].casefold():
        sys.exit(
            "initial sequence does not match mutation description (check 'from' field"
        )
    else:
        initial_array[mut_dic["position"]] = mut_dic["to"]
    resulting_seq = array_to_seq(initial_array, gapped=False)
    # add mutation description to sequence name
    resulting_seq.name = initial_seq.name + "_" + mutation_string
    resulting_seq.id = initial_seq.id + "_" + mutation_string
    return resulting_seq


def dna_mutation_translate(initial_seq, mutation_string):
    """
    Decode a DNA mutation into protein mutation

    Parameters
    --------------
    initial_seq: SeqRecord
        a sequence to be modified
    mutation_string: str
        a string describing DNA substitution using colloquial notation e.g. A83C

    Return
    --------------
    str: a string encoding protein mutation
    """
    mutated_seq = make_point_mutation(initial_seq, mutation_string)
    res = compare_two_seq_and_annotate_point_mutation(
        initial_seq.translate(), mutated_seq.translate()
    )
    return res[0]


# dna_mutation_translate(gene,"A68C")


def get_all_codons_for_aa(amino_acid_single, ambiguous=False):
    """
    Shows all dna codons for an amino acid. 
    
    Note:
    -------------- 
    This function is using bacterial codon table!

    Parameters
    --------------
    amino_acid_single: str
        an amino acid using a single letter notation, i.e. A for alanine or W for triptophane
    ambiguous: logic
        also show ambiguos nucleotide notation, for example W for A/T. Default=False

    Return
    --------------
    list: a list of nucleotide codons 
    """
    forward_table = CodonTable.ambiguous_dna_by_name[
        "Bacterial"
    ].forward_table.forward_table
    res = []
    for codon, aa in forward_table.items():
        if aa == amino_acid_single:
            res.append(codon)
    if ambiguous:
        return CodonTable.list_ambiguous_codons(
            res, IUPAC.IUPACData.ambiguous_dna_values
        )
    else:
        return res


# get_all_codons_for_aa('R')
# get_all_codons_for_aa('R',ambiguous=True)


def get_dist_arrays(array1, array2):
    """
    Calculate a number of differences between two arrays 
    
    Parameters
    --------------
    array1: numpy array
        
    array2: numpy array

    Return
    --------------
    int: a numer of differences between thwo arrays
    """
    array_to_comp = np.array([array1, array2])
    return len(get_diff_columns(array_to_comp))


def get_all_codons_for_aa_mutation(initial_dna_seq, aa_mutation):
    """
    Shows all codons which would result into amino acid substitution provided. The codons are sorted based on their distance to initail sequence.  
    
    Note:
    -------------- 
    This function is using bacterial codon table!

    Parameters
    --------------
    initial_dna_seq: SeqRecord
        DNA sequence to be modified
    aa_mutation: str
        a protein mutation encoded using colloquial notation e.g. L22R

    Return
    --------------
    list: a list of dictionaries. Each dictionary describe a new codon and its distance to the original codon. The codons are sorted based on their distance to initail sequence.  
    """
    # convert mutation string to dictionary
    mut_dic = mutation_string_to_dic(aa_mutation)
    # convert dna sequence to aa
    initial_array_aa = seq_to_numpy(initial_dna_seq.translate())
    if initial_array_aa[mut_dic["position"]].casefold() != mut_dic["from"].casefold():
        sys.exit("from field in mutation string does not match initial sequence")
    # slice a codon to be modified
    initial_array_dna = seq_to_numpy(initial_dna_seq)
    from_array = initial_array_dna[
        slice(mut_dic["position"] * 3, mut_dic["position"] * 3 + 3, 1)
    ]
    from_ = array_to_str(from_array)
    # obtain all possible codons resulting into substitution
    all_possible_codons = get_all_codons_for_aa(mut_dic["to"], ambiguous=False)
    # get all possible DNA mutation resulting into aa substitution
    res = []
    for acodon in all_possible_codons:
        to = str(acodon)
        dist = get_dist_arrays(from_array, str_to_numpy(to))
        res.append(
            {
                "from_": from_,
                "position0": mut_dic["position"] * 3,
                "to": to,
                "dist": dist,
            }
        )
    res2 = sorted(res, key=itemgetter("dist"), reverse=False)
    for adic in res2:
        print(
            "{}{}{} distance: {}".format(
                adic["from_"], adic["position0"] + 1, adic["to"], adic["dist"]
            )
        )
    return res2


def compare_two_seq_and_annotate_point_mutation(initial_seq, mutated_seq):
    """
    Compare two sequences and report their differences as a list of mutation strings
    
    Parameters
    --------------
    initial_seq: SeqRecord
        intitial sequence to be used as a reference
    initial_seq: SeqRecord
        modified sequence 

    Return
    --------------
    A list of mutations in colloquial format, e.g. L28R or A83T 
    """
    # align two seq and convert to array
    array = alignment_to_numpy(get_alignment_mafta(initial_seq, mutated_seq))
    # get list of diff positions
    diff_columns = get_diff_columns(array)
    # annotate point mutations
    res = []
    for amut in diff_columns:
        from_ = array[0, int(amut)]
        to = array[1, int(amut)]
        position = int(amut) + 1
        res.append(str(from_) + str(position) + str(to))
    return res


# get_all_codons_for_aa('R')
# get_all_codons_for_aa('R',ambiguous=True)
# get_all_codons_for_aa_mutation(gene,'L28R')
# get_all_codons_for_aa_mutation(gene,'L28V')
