#!/usr/bin/env python3
# -*- coding: utf-8 -*-

# generic
import itertools
import numpy
from operator import itemgetter

# bio
from Bio.SeqUtils import GC
import seqfold

# custom
from templatelib.helpers.numpy_wrappers import *


def create_primer_variants(
    seq, mutation_position, min_max_size=[45, 55], mutation_position_flexibility=5
):
    """
    Create a list of sequences to choose a primer based on minimal and maximal size of a primer, a position of mutatiflexibilityon within a primer and allowed variation from this position. At this point, there is no filtering by any other parameters
    """
    min_max_dist_end = [i // 2 for i in min_max_size]
    min_max_dist_end[0] = min_max_dist_end[0] - mutation_position_flexibility
    min_max_dist_end[1] = min_max_dist_end[1] + mutation_position_flexibility
    min_pos = int(mutation_position - min_max_dist_end[1])
    max_pos = int(mutation_position + min_max_dist_end[1])
    full_array = str_to_numpy(seq.seq.lower())
    full_array[mutation_position] = full_array[mutation_position].upper()
    iteration_intervals = list(itertools.combinations(range(min_pos, max_pos), 2))
    chosen_int = []
    for interval in iteration_intervals:
        if (numpy.diff(interval)[0] >= min_max_size[0]) & (
            numpy.diff(interval)[0] <= min_max_size[1]
        ):
            chosen_int.append(slice(interval[0], interval[1], 1))
    seq_list = []
    for aslice in chosen_int:
        seq_list.append(array_to_seq(full_array[aslice]))
    return seq_list


def analyze_primer(primer_rec):
    """
    Takes a sequence record for a primer and calculates dG (seqfold), Tm, GC%,  length and returns as a dictionary 
    """
    sequence = str(primer_rec.seq)
    dG = seqfold.dg(sequence)
    Tm = seqfold.tm(sequence)
    GC_perc = GC(sequence)
    len_ = len(sequence)
    res = {"sequence": sequence, "dG": dG, "Tm": Tm, "GC_perc": GC_perc, "len_": len_}
    return res


def print_list_primers(list_of_primers):
    """
    Takes a list of primer sequence records (returned by create primer variants) and prints their characteriastics (dG, Tm, GC%,)
    """
    for aprimer in list_of_primers:
        primer_info = analyze_primer(aprimer)
        print(
            "{: .1f}\t{: .1f}\t{: .1f}\t{: .0f}\t{}".format(
                primer_info["dG"],
                primer_info["Tm"],
                primer_info["GC_perc"],
                primer_info["len_"],
                primer_info["sequence"],
            )
        )


def print_primers_analysed(list_of_primers_analysed):
    for primer_info in list_of_primers_analysed:
        print(
            "{: .1f}\t{: .1f}\t{: .1f}\t{: .0f}\t{}".format(
                primer_info["dG"],
                primer_info["Tm"],
                primer_info["GC_perc"],
                primer_info["len_"],
                primer_info["sequence"],
            )
        )


def analyze_list_primer(list_of_primers):
    """
    Take a list of primers, analyze (dG, Tm, GC%,len) them and return a list of dictionaries with results
    """
    list_analysed = [analyze_primer(i) for i in list_of_primers]
    return list_analysed


def rank_primers(list_analysed, list_of_param=["len_", "dG"], order_incr=[False, True]):
    """
    Take a list of dictionaries with analyzed primers and rank them acorrding to chosen parameters. 
    """
    for n in range(0, len(list_of_param)):
        list_ranked = sorted(
            list_analysed, key=itemgetter(list_of_param[n]), reverse=order_incr[n]
        )
    return list_ranked


def analysed_list_primers_to_seq(list_analysed):
    """
    Take a list of dictionaries with analyzed primers and return them as a list of sequence records
    """
    seq_list = []
    for n in range(0, len(list_analysed)):
        primer_info = list_analysed[n]
        seq_list.append(
            SeqRecord(
                Seq(primer_info["sequence"]),
                id="temp" + str(n + 1),
                description="dG={: .1f}, Tm={: .1f}, GC%={: .1f}, len={: .0f}".format(
                    primer_info["dG"],
                    primer_info["Tm"],
                    primer_info["GC_perc"],
                    primer_info["len_"],
                ),
            )
        )
    return seq_list


# list_of_primers_analysed[0]


def calculate_abs_diff(avalue, optimal_value, coef):
    difference = numpy.diff(numpy.array([avalue, optimal_value]))[0]
    return coef * abs(difference)


def get_optimal_value():
    dic_opt = {"GC_perc": 50, "dG": 5, "len_": 80}
    return dic_opt


def get_coef():
    dic_opt = {"GC_perc": 1, "dG": 3, "len_": 1}
    return dic_opt


def calculate_penalty(primer_dic, optimal_value_dic, penalty_coef_dic):
    total_sum = {}
    for aparam in ["GC_perc", "dG", "len_"]:
        penalty = calculate_abs_diff(
            avalue=primer_dic[aparam],
            optimal_value=optimal_value_dic[aparam],
            coef=penalty_coef_dic[aparam],
        )
        total_sum[aparam] = penalty

    return sum(total_sum.values())


# calculate_penalty(list_of_primers_analysed[0],get_optimal_value(),get_coef())
