#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import subprocess
from io import StringIO
from Bio import SearchIO


def local_blast(query_path, target_seq):
    command = "/media/DATA/mol/lib/blast_shortseq_to_fasta_xml.sh"
    results = subprocess.run([command, query_path, target_seq], stdout=subprocess.PIPE)
    return results


def get_alignment_blast(query_path, target_seq):

    blast_out = local_blast(query_path, target_seq)
    blast_query = StringIO()
    blast_query.write(blast_out.stdout.decode())
    blast_query.seek(0)
    qresult = SearchIO.read(blast_query, "blast-xml")
    print(qresult)
    blast_query.close
    print(qresult[0].fragments[0].aln)
    return qresult[0].fragments[0].aln


# aa=get_alignment_blast(fasta_gene,str(gRNA_seq_modified_pam))
