"""
This script was used to cnvert a codon distance matrix as dictionary (in a pickle).
A simple Hamming distnase was used
"""

import pkg_resources
import csv
import pickle

# get path to the data distributed with the package
filepath = pkg_resources.resource_filename('templatelib', 'data/codon_dist_long.csv')

mydict={}

# read as a library
with open(filepath, mode='r') as infile:
    for cod1,cod2,dist in csv.reader(infile):
        if cod1 != 'V1':
            mydict.update( {cod1+'_'+cod2 : int(dist)})





def save_object(obj, filename):
    """
    Save object as a picklefile

    Parameters
    --------------
    obj: object
    filename: str
        output file path
        
    """
    with open(filename, 'wb') as output:  # Overwrites any existing file.
        pickle.dump(obj, output, pickle.HIGHEST_PROTOCOL)

# save dictionary as pickle 
save_object(mydict, 'out_data/codon_dist_long.pkl')
