#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import tempfile
import subprocess
from io import StringIO
from Bio import AlignIO


def get_alignment_mafta(query_seq, target_seq):
    """
    Align two sequences using mafft 

    Parameters
    --------------
    query_seq: SeqRecord 
        query sequence 
    target_seq: SeqRecord
        target sequence
    
    Return
    --------------
    Aligned sequences as Biopython Alignment object

    """

    # create temp fasta file
    fp = tempfile.NamedTemporaryFile(mode="w", suffix=".fasta")
    fp.write(query_seq.format("fasta"))
    fp.write(target_seq.format("fasta"))
    fp.seek(0)

    # run mafft
    res = subprocess.run(
        ["mafft", "--localpair", "--adjustdirection", "--quiet", fp.name],
        stdout=subprocess.PIPE,
    )
    fp.close()

    # parse mafft result as alignment object
    al_res = StringIO()
    al_res.write(res.stdout.decode())
    al_res.seek(0)
    return AlignIO.read(al_res, "fasta")


# get_alignment_mafta(seq_rec1,seq_rec2)
