# templatelib

A collection of python scripts to design oligonucleotide templates for gene editing

## Functionality

The collection include two key functionalities:


- incorporate DNA mutations into oligonucleotides:
    - point mutations
    - deletion/insertions
    - point mutations can also be specified as amino acid sequence. In this case, E. coli codon usage will be used to select a codon
    
- design optimal oligonucleotides by taking into account:
    - nucleotide length
    - GC content 
    - secondary structury ($`\Delta G`$)
    - substitution distance to the centre of an oligonucleotide

## Algorithm to design oligonucleotides
1. Select a target position in the genome to be mutated.
2. Create a collection of oligonucleotides spanning the target position such that their length is within a given range, and the middle of oligonucleotide sequence is at most N nucleotides from the target position
3. Compute GC content, length, secondary structure (ΔG) of oligonucleotides. ΔG is calculated using seqfold 
4. Assuming optimal parameters are: GC content = 50%, length = 80 bases and ΔG = 5 kcal/mole, the oligonucleotides are scored by penalizing deviations from the optimal parameters. Penalty coefficients were 1 for a 1% difference in GC content; 3 for –1 kcal/mole decrease in ΔG; and 1 for 1 nucleotide increase in length.
5. Rank oligonucleotides based on the sum of penalty scores and select the top-ranked oligonucleotide.


## Dependencies

- Python >= 3.8
- Biopython >= 1.79 
- numpy >=1.22.4
- seqfold (alternative for mfold, UNAfold) >= 0.7.14
- pandas >= 1.4.2
- pydna (will be removed in the future)
- primer3 (will be removed in the future)

## Intstallation 

```
pip3 install --upgrade git+git://gitlab.com/apson/templatelib.git
```

Currently, it is a privite repository and can be only installed by me issuing:

```
pip3 install git+ssh://git@gitlab.com/apson/templatelib.git --user

```

## Usage

```
$ get_oligos_for_editing.py --help
usage: Make primers with mismatch mutations PAM for genetic editing [-h] [--fasta-gRNA FILE]
                                                                    [--fasta-gene FILE]
                                                                    [--gRNA STR]
                                                                    [--modified-pam STR]
                                                                    [--dna-mutation STR]
                                                                    [--min-max-size MIN,MAX]
                                                                    [--flexibility INT]

optional arguments:
  -h, --help            show this help message and exit
  --fasta-gRNA FILE     path to fasta file with one or more gRNAs (including PAM sequence at 3 prime end
  --fasta-gene FILE     path to gene sequence to be edited in fasta format
  --gRNA STR            a name of gRNA as in the fasta file, e.g. folA_gR38f
  --modified-pam STR    a three-letter sequence of destroyed PAM sequence. In PAM is not be
                        modified, provide the unmodified sequence
  --dna-mutation STR    a DNA mutation in a colloquial format, eg. T83G
  --min-max-size MIN,MAX
                        size of oligos given as min and max value separated by comma, eg.
                        45,46
  --flexibility INT     a maximum number of nucleotide postions from oligo's center the
                        mutation can be located

```







